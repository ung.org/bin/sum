/*
 * UNG's Not GNU
 *
 * Copyright (c) 2011-2019, Jakob Kaivo <jkk@ung.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define _POSIX_C_SOURCE 2
#include <errno.h>
#include <locale.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

static int sum(const char *path, int alt)
{
	intmax_t bytes = 0;

	FILE *f = stdin;
	if (path && strcmp(path, "-")) {
		f = fopen(path, "r");
	}

	if (f == NULL) {
		fprintf(stderr, "sum: %s: %s\n", path, strerror(errno));
		return 1;
	}

	int c;
	unsigned int s = 0;
	while ((c = fgetc(f)) != EOF) {
		if (alt) {
			s = (s >> 1) + ((s & 1) << 15);
		}

		s += c;

		if (alt) {
			s &= 0xffff;
		}
		bytes++;
	}

	s = (s & 0xffff) + (s >> 16);

	if (bytes % 512 != 0) {
		bytes += 512;
	}
	printf("%u %zd", s, bytes / 512);
	if (f != stdin) {
		printf(" %s", path);
		fclose(f);
	}
	putchar('\n');

	return 0;
}

int main(int argc, char *argv[])
{
	setlocale(LC_ALL, "");
	fprintf(stderr, "sum: utility is obsolete; use cksum\n");

	int alt = 0;

	int c;
	while ((c = getopt(argc, argv, "r")) != -1) {
		switch (c) {
		case 'r':
			alt = 1;
			break;

		default:
			return 1;
		}
	}

	int r = 0;
	do {
		r |= sum(argv[optind++], alt);
	} while (optind < argc);
	return r;
}
